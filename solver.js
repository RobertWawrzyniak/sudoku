"use strict";

function solve(unsolved_sudoku) {
    const matrix = transform(unsolved_sudoku);
    const history = [];
    let number = 1;
    while (true) {
        const [isDone, nextNumber] = step(matrix, history, number);
        number = nextNumber;
        if (isDone) {
            return retransform(matrix);
        }
    }
}

function step(matrix, history, oldNumber) {
    // returns [isDone, numberToCheck]
    let coordinates = findNext(matrix);
    if (coordinates == null) {
        return [true, null];
    }

    const result = checkAllNumbersInCell(matrix, history, oldNumber, coordinates);
    if (result !== null) {
        return result;
    }

    const [x, y] = history.pop();
    const prevNumber = matrix[y][x];
    matrix[y][x] = null;

    return [false, prevNumber + 1];
}

function checkAllNumbersInCell(matrix, history, oldNumber, coordinates) {
    for(let number = oldNumber; number <= 9; number++) {
        const isValid = checkMatrix(matrix, coordinates, number);
        if (isValid) {
            history.push(coordinates);
            const [x, y] = coordinates;
            matrix[y][x] = number;
            return [false, 1];
        }
    }
    return null;
}

function transform(unsolved_sudoku) {
    const table = [];
    for (let x = 0; x < 9; x++) {
        const row = [];
        for (let y = 0; y < 9; y++) {
            const index = y + x * 9;
            const rawNumber = unsolved_sudoku[index];
            const number = rawNumber === '_' ? null : parseInt(rawNumber);
            row.push(number);
        }
        table.push(row);
    }
    return table;
}

function retransform(matrix) {
    return matrix.map(row => 
        row.map(e => e == null ? '_' : e + '').join('')
    ).join('');
}

function checkMatrix(matrix, cords, number) {
    return (
        checkRow(matrix, cords, number)
        && checkCol(matrix, cords, number)
        && checkBox(matrix, cords, number)
    );
}

function checkRow(matrix, cords, number) {
    const y = cords[1];
    const row = matrix[y];
    return !row.some(n => n == number);
}

function checkCol(matrix, cords, number) {
    const x = cords[0];
    return !matrix.some(row => row[x] == number);
}

function checkBox(matrix, cords, number) {
    const [indX, indY] = cords;
    const boxX = Math.floor(indX / 3);
    const boxY = Math.floor(indY / 3);
    for(let px=0; px < 3; px++) {
        for(let py=0; py < 3; py++) {
            const x = boxX * 3 + px;
            const y = boxY * 3 + py;
            let matrixNumber = matrix[y][x];
            if (matrixNumber == number) {
                return false;
            }
        }
    }
    return true;
}

function findNext(matrix) {
    for (let x = 0; x < 9; x++) {
        for (let y = 0; y < 9; y++) {
            const matrixNumber = matrix[y][x];
            if (matrixNumber === null) {
                return [x, y];
            }
        }
    }

    return null;
}

exports.solve = solve;
exports.checkRow = checkRow;
exports.checkCol = checkCol;
exports.checkBox = checkBox;
exports.transform = transform;
exports.retransform = retransform;
exports.findNext = findNext;
exports.step = step;