"use strict";

const chai = require("chai");
require("mocha");
const solver = require("./solver");

const n = null;
const matrix = [
    [n, n, n, 2, 6, n, 7, n, 1],
    [6, 8, n, n, 7, n, n, 9, n],
    [1, 9, n, n, n, 4, 5, n, n],
    [8, 2, n, 1, n, n, n, 4, n],
    [n, n, 4, 6, n, 2, 9, n, n],
    [n, 5, n, n, n, 3, n, 2, 8],
    [n, n, 9, 3, n, n, n, 7, 4],
    [n, 4, n, n, 5, n, n, 3, 6],
    [7, n, 3, n, 1, 8, n, n, n],
];

describe("Sudoku one", function () {
    it("should be solved", function () {
        var unsolved_sudoku = "___26_7_168__7__9_19___45__82_1___4___46_29___5___3_28__93___74_4__5__367_3_18___";
        var solved = "435269781682571493197834562826195347374682915951743628519326874248957136763418259";
        chai.assert.equal(solver.solve(unsolved_sudoku), solved);
    });
});

describe("transform to matrix", () => {
    it("should string properly transform into matrix", () => {
        var unsolved_sudoku = "___26_7_168__7__9_19___45__82_1___4___46_29___5___3_28__93___74_4__5__367_3_18___";
        var result = solver.transform(unsolved_sudoku);
        chai.assert.deepEqual(result, matrix);
    });
});

describe("retransform to string", () => {
    it("should matrix properly retransform into string", () => {
        var expected = "___26_7_168__7__9_19___45__82_1___4___46_29___5___3_28__93___74_4__5__367_3_18___";
        var result = solver.retransform(matrix);
        chai.assert.deepEqual(result, expected);
    });
});

describe("validating", () => {
    it("for duplicated values in row should return false", () => {
        const isValid = solver.checkRow(matrix, [2, 0], 2);
        chai.assert.isFalse(isValid);
    });
    it("for duplicated values in row should return true", () => {
        const isValid = solver.checkRow(matrix, [2, 0], 3);
        chai.assert.isTrue(isValid);
    });
    it("for duplicated values in col should return false", () => {
        const isValid = solver.checkCol(matrix, [2, 0], 4);
        chai.assert.isFalse(isValid);
    });
    it("for duplicated values in col should return true", () => {
        const isValid = solver.checkCol(matrix, [2, 0], 5);
        chai.assert.isTrue(isValid);
    });
    it("for duplicated values in box should return false", () => {
        const isValid = solver.checkBox(matrix, [2, 0], 1);
        chai.assert.isFalse(isValid);
    });
    it("for duplicated values in box should return true", () => {
        const isValid = solver.checkBox(matrix, [2, 0], 7);
        chai.assert.isTrue(isValid);
    });
});

describe("compute next cords", () => {
    it("should return coordinates of first empy place found", () => {
        const coordinates = solver.findNext(matrix);
        chai.assert.deepEqual(coordinates, [0, 0]);
    });
})

describe("step", () => {
    it("should insert value if can", () => {
        const oldMatrix = [
            [n, n, n, 2, 6, n, 7, n, 1],
            [6, 8, n, n, 7, n, n, 9, n],
            [1, 9, n, n, n, 4, 5, n, n],
            [8, 2, n, 1, n, n, n, 4, n],
            [n, n, 4, 6, n, 2, 9, n, n],
            [n, 5, n, n, n, 3, n, 2, 8],
            [n, n, 9, 3, n, n, n, 7, 4],
            [n, 4, n, n, 5, n, n, 3, 6],
            [7, n, 3, n, 1, 8, n, n, n],
        ];
        const history = [];

        const [nextStep, num] = solver.step(oldMatrix, history, 1);
        chai.assert.equal(nextStep, false);
        chai.assert.equal(oldMatrix[0][0], 3);
        chai.assert.equal(history.length, 1);
        chai.assert.deepEqual(history[0], [0, 0]);
    });
})